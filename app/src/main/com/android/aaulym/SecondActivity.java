package com.example.aaulym;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


public class SecondActivity extends ActionBarActivity {
    TextView titleOfLesson,levelOfLesson;
   // ProgressBar progressBar;
    Button back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        titleOfLesson = (TextView)findViewById(R.id.titleCardView);
        levelOfLesson= (TextView)findViewById(R.id.levelOfLesson);
        //progressBar=(ProgressBar)findViewById(R.id.progressBar2);
        back =(Button)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecondActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        if(b!=null)
        {
            String j =(String) b.get("title");
            String k =(String) b.get("level");
            //int l=(Integer)b.get("progressStatus");
            titleOfLesson.setText(j);
            levelOfLesson.setText(k);
          //  progressBar.setProgress(l);
        }
    }
}
