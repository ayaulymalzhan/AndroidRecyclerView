package com.example.aaulym;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class CardViewFragment extends Fragment {
    TextView titleOfLesson,levelOfLesson;
    Button back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_card_view, container, false);
        titleOfLesson = (TextView)v.findViewById(R.id.titleCardView);
        levelOfLesson= (TextView)v.findViewById(R.id.levelOfLesson);
        //progressBar=(ProgressBar)findViewById(R.id.progressBar2);
        String titleStr =getArguments().getString("title");
        String levelStr =getArguments().getString("level");
        titleOfLesson.setText(titleStr);
        levelOfLesson.setText(levelStr);
//        back =(Button)v.findViewById(R.id.back);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(),MainActivity.class);
//                startActivity(intent);
//
//            }
//        });

        return v;
    }





    @Override
    public void onDetach() {
        super.onDetach();

    }



}
