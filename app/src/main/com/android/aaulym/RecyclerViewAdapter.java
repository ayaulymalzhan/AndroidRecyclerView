package com.example.aaulym;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

    private List<ItemObject> itemList;
    private Context context;
    String titleStr,levelStr;
    int progressStatus =0;


    public RecyclerViewAdapter(Context context, List<ItemObject> itemList) {
        this.itemList = itemList;
        this.context = context;

    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {
        holder.Title.setText(itemList.get(position).getTitle());
        holder.Level.setText(itemList.get(position).getLevel());
        holder.Photo.setImageResource(itemList.get(position).getPhotoId());
        holder.NextButton.setImageResource(itemList.get(position).getNextId());
        holder.progressBar.setProgress(itemList.get(position).getProgressBarId());
        holder.NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleStr=holder.Title.getText().toString();
                levelStr=holder.Level.getText().toString();
                Bundle extras = new Bundle();
                extras.putString("title",titleStr);
                extras.putString("level",levelStr);
                MainActivity activity = (MainActivity)view.getContext();
                android.support.v4.app.FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                CardViewFragment fragment = new CardViewFragment();
                fragment.setArguments(extras);

                transaction.replace(R.id.container, fragment);
                transaction.commit();

                //progressStatus=holder.progressBar.getProgress();
//                Intent i=new Intent(context,SecondActivity.class);

//                //extras.putInt("progressSatus",progressStatus);
//                i.putExtras(extras);
//                context.startActivity(i);
//                CardViewFragment cvFrag =new CardViewFragment();
//
//                int id = (int) getItemId(position);
//                switch (id){
//                    case 0:
//
//                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}
