package com.example.aaulym;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class RecyclerViewHolders extends RecyclerView.ViewHolder  {

    public TextView Title,Level;
    public ImageView Photo,NextButton;
    ProgressBar progressBar;
    int progressStatus=0;
  //  private final Context context;


    public RecyclerViewHolders(final View itemView) {
        super(itemView);


        Title = (TextView)itemView.findViewById(R.id.title);
        Level = (TextView)itemView.findViewById(R.id.level);
        Photo = (ImageView)itemView.findViewById(R.id.car);
        NextButton = (ImageView) itemView.findViewById(R.id.nextbutton);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        progressBar.setProgress(progressStatus);

//        context = itemView.getContext();

    }



}



