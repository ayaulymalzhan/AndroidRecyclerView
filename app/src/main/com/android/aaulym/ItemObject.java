package com.example.aaulym;

/**
 * Created by aaulym on 27.05.17.
 */

class ItemObject {

    private String title,level;
    private int photoId,next,progressBarStatus;

    public ItemObject(String title, String level, int photoId,int next,int progressBarStatus) {
        this.title = title;
        this.level = level;
        this.photoId = photoId;
        this.next=next;
        this.progressBarStatus=progressBarStatus;

    }

    public String getTitle() {
        return title;
    }


    public String getLevel() {
        return level;
    }

    public int getPhotoId() {
        return photoId;
    }
    public int getNextId() {
        return next;
    }
    public int getProgressBarId(){
        return progressBarStatus;
    }

}
