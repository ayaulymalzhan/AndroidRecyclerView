package com.example.aaulym;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;



public class MainActivity extends ActionBarActivity {

    private LinearLayoutManager lLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(null);

        Toolbar topToolBar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
        topToolBar.setLogo(R.drawable.drivingschoollogo);
        topToolBar.setLogoDescription(getResources().getString(R.string.logo_desc));

        List<ItemObject> rowListItem = getAllItemList();
        lLayout = new LinearLayoutManager(MainActivity.this);

        RecyclerView rView = (RecyclerView)findViewById(R.id.recycler_view);
        rView.setLayoutManager(lLayout);

        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(MainActivity.this, rowListItem);
        rView.setAdapter(rcAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<ItemObject> getAllItemList(){

        List<ItemObject> allItems = new ArrayList<ItemObject>();
        allItems.add(new ItemObject("Introduction to Driving", "Beginner", R.drawable.caricons,R.drawable.next,30));
        allItems.add(new ItemObject("Observation at Junctions", "Beginner", R.drawable.caricons,R.drawable.next,30));
        allItems.add(new ItemObject("Reverse Parallel Parking", "Intermediate", R.drawable.caricons,R.drawable.next,50));
        allItems.add(new ItemObject("Reversing around Corner", "Intermediate", R.drawable.caricons,R.drawable.next,50));
        allItems.add(new ItemObject("Incorrect Use Of Signals", "Advanced", R.drawable.caricons,R.drawable.next,100));

        allItems.add(new ItemObject("Observation at Junctions", "Beginner", R.drawable.caricons,R.drawable.next,30));
        allItems.add(new ItemObject("Reverse Parallel Parking", "Intermediate", R.drawable.caricons,R.drawable.next,50));
        allItems.add(new ItemObject("Introduction to Driving", "Beginner", R.drawable.caricons,R.drawable.next,30));
        allItems.add(new ItemObject("Reversing around Corner", "Intermediate", R.drawable.caricons,R.drawable.next,50));
        allItems.add(new ItemObject("Incorrect Use Of Signals", "Advanced", R.drawable.caricons,R.drawable.next,100));

        return allItems;
    }
}
